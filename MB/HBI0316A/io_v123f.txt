BOARD: HBI0316A
TITLE: N1SDP IOFPGA Configuration File

[FPGAS]
TOTALFPGAS: 1               ;Total Number of FPGAs
F0FILE: io_v123f.bit        ;FPGA0 Filename
F0MODE: FPGA                ;FPGA0 Programming Mode

[PMIC]
MBPMIC: pms_0V85.bin        ;MB PMIC for boards before 36253xxx
;MBPMIC: 300k_8c2.bin        ;MB PMIC for boards after 36253xxx

[OSCCLKS]
TOTALOSCCLKS: 12
OSC0:  50.0                 ;OSC0-Y5 - SYS_REF_CLK
OSC1:  50.0                 ;OSC0-Y4 - CPU0_REF_CLK
OSC2:  50.0                 ;OSC0-Y6 - CPU1_REF_CLK
OSC3:  50.0                 ;OSC1-Y2 - CLUS_REF_CLK
OSC4:  50.0                 ;OSC1-Y4 - INT_REF_CLK
OSC5:  50.0                 ;OSC1-Y6 - REF_CLK
OSC6:  50.0                 ;OSC2-Y2 - DMC_REF_CLK
OSC7:  23.75                ;OSC2-Y4 - IOFPGA_PXLCLK (HDLCD)
OSC8:  80.0                 ;OSC2-Y6 - IOFPGA_TXLCLK
OSC9:  60.0                 ;OSC3-Y2 - IOFPGA_ACLK
OSC10: 24.576               ;OSC3-Y4 - IOFPGA_AUDCLK
OSC11: 24.0                 ;OSC3-Y6 - IOFPGA_RSVDCLK

[HARDWARE CONTROL]
;ASSERTNPOR: TRUE            ;External resets assert nPOR !TBA

[PERIPHERAL SUPPORT]
FPGA_SMB: TRUE              ;SMB interface is supported (MCC_SMC<>FPGA_SMB)

FPGA_SCC: TRUE              ;SCC interface is supported
SCCREG:  0x68130000         ;SCC registers base address

FPGA_DDR: TRUE              ;DDR interface is supported
DDRBASE: 0x68040000         ;DDR I2C register address

FPGA_SYSREG: TRUE           ;System register interface is supported
FPGAREG: 0x68010000         ;System registers base address

FPGA_HDMI: TRUE             ;HDMI interface is supported
HDMIBASE: 0x680F0000        ;HDMI I2C register address

FPGA_LAN: TRUE              ;LAN LAN9220 interface is supported
LANBASE: 0x69100000         ;LAN LAN9220 base address

FPGA_RTC: TRUE              ;RTC PL031 interface is supported
RTCBASE: 0x68100000         ;RTC PL031 base address

FPGA_QSPI: TRUE             ;QSPI interface is supported
QSPIBASE: 0x680C0000        ;QSPI controller base address
QSPIDATA: 0x64000000        ;QSPI data address
;XIPBASE: 0x65800000         ;QSPI XIP controller base address !TBA
;QSPISCC: 0x08               ;QSPI SCC register !TBA

C2C_ENABLE: TRUE            ;C2C enable TRUE/FALSE
C2C_SIDE: SLAVE             ;C2C side SLAVE/MASTER

[SCC REGISTERS]
TOTALSYSCONS: 13            ;Total Number of SCC registers defined
SYSCON: 0x000C 0x00000003   ;IOFPGA SCC_MCC_MBS_ADDR

SOCCON: 0x1160 0x00000001   ;SoC SCC BOOT_CTL - enable TLX
SOCCON: 0x1164 0x01000000   ;SoC SCC BOOT_CTL_STA (0xX1000000 = MCC OK)
SOCCON: 0x1168 0x14000000   ;SoC SCC SCP_BOOT_ADR
SOCCON: 0x116C 0x16000000   ;SoC SCC MCP_BOOT_ADR
;SOCCON: 0x1170 0x00000000   ;SoC SCC PLATFORM_CTRL
;SOCCON: 0x1174 0x00000000   ;SoC SCC TARGETIDAPP (0x07B00477)
;SOCCON: 0x1178 0x00000000   ;SoC SCC TARGETIDSCP (0x07B10477)
;SOCCON: 0x117C 0x00000000   ;SoC SCC TARGETIDMCP (0x07B20477)
SOCCON: 0x1180 0x00000000   ;SoC SCC BOOT_GPR0
SOCCON: 0x1184 0x00000000   ;SoC SCC BOOT_GPR1
SOCCON: 0x1188 0x00000000   ;SoC SCC BOOT_GPR2
SOCCON: 0x118C 0x00000000   ;SoC SCC BOOT_GPR3
SOCCON: 0x1190 0x00000000   ;SoC SCC BOOT_GPR4
SOCCON: 0x1194 0x00000000   ;SoC SCC BOOT_GPR5
SOCCON: 0x1198 0x00000000   ;SoC SCC BOOT_GPR6
SOCCON: 0x119C 0x00000000   ;SoC SCC BOOT_GPR7
